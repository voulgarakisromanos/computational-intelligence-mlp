from keras.datasets import mnist
import numpy as np
from matplotlib import pyplot
import tensorflow as tf
(train_image, train_class), (val_image, val_class) = mnist.load_data()

extra_val_image = train_image[0:12000]
extra_val_class = train_class[0:12000]
np.append(val_image, extra_val_image)
np.append(val_class, extra_val_class)
train_image_used = train_image[12000:]
train_class_used = train_class[12000:]
epoch_number = 100

model1_1 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])

model1_1.compile(
    optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.001, rho=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history1_1 = model1_1.fit(train_image_used, train_class_used, batch_size=1, epochs=5, validation_data=(val_image, val_class))

model1_256 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])

model1_256.compile(
    optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.001, rho=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history1_256 = model1_256.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))

model1_full_batch = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])

model1_full_batch.compile(
    optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.001, rho=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history1_full_batch = model1_full_batch.fit(train_image_used, train_class_used, batch_size=48000, epochs=epoch_number, validation_data=(val_image, val_class))

model2 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])
model2.compile(
    optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.001, rho=0.99),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history2 = model2.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))
#
gaussian_init = tf.keras.initializers.RandomNormal(mean=10, stddev=1)

model3 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, kernel_initializer=gaussian_init, activation='relu'),
    tf.keras.layers.Dense(256, kernel_initializer=gaussian_init, activation='relu'),
    tf.keras.layers.Dense(10, kernel_initializer=gaussian_init, activation='softmax')
])
model3.compile(
    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history3 = model3.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))


regularizer1 = tf.keras.regularizers.L2(0.1)
regularizer2 = tf.keras.regularizers.L2(0.01)
regularizer3 = tf.keras.regularizers.L2(0.001)

model4 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, kernel_initializer=gaussian_init, kernel_regularizer=regularizer1, activation='relu'),
    tf.keras.layers.Dense(256, kernel_initializer=gaussian_init, kernel_regularizer=regularizer1, activation='relu'),
    tf.keras.layers.Dense(10, kernel_initializer=gaussian_init, kernel_regularizer=regularizer1, activation='softmax')
])
model4.compile(
    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history4 = model4.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))

model5 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, kernel_initializer=gaussian_init, kernel_regularizer=regularizer2, activation='relu'),
    tf.keras.layers.Dense(256, kernel_initializer=gaussian_init, kernel_regularizer=regularizer2, activation='relu'),
    tf.keras.layers.Dense(10, kernel_initializer=gaussian_init, kernel_regularizer=regularizer2, activation='softmax')
])
model5.compile(
    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history5 = model5.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))

model6 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, kernel_initializer=gaussian_init, kernel_regularizer=regularizer3, activation='relu'),
    tf.keras.layers.Dense(256, kernel_initializer=gaussian_init, kernel_regularizer=regularizer3, activation='relu'),
    tf.keras.layers.Dense(10, kernel_initializer=gaussian_init, kernel_regularizer=regularizer3, activation='softmax')
])
model6.compile(
    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history6 = model6.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))

model7 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, kernel_initializer=gaussian_init, kernel_regularizer=regularizer3, activation='relu'),
    tf.keras.layers.Dense(256, kernel_initializer=gaussian_init, kernel_regularizer=regularizer3, activation='relu'),
    tf.keras.layers.Dense(10, kernel_initializer=gaussian_init, kernel_regularizer=regularizer3, activation='softmax')
])
model7.compile(
    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history7 = model7.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))

regularizer4 = tf.keras.regularizers.L1(0.01)

model8 = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, kernel_regularizer=regularizer4, activation='relu'),
    tf.keras.layers.Dense(256, kernel_regularizer=regularizer4, activation='relu'),
    tf.keras.layers.Dense(10, kernel_regularizer=regularizer4, activation='softmax'),
    tf.keras.layers.Dropout(rate=0.3)
])

model8.compile(
    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)

history8 = model8.fit(train_image_used, train_class_used, batch_size=256, epochs=epoch_number, validation_data=(val_image, val_class))


# for i in range(25):
#     pyplot.subplot(5, 5, i + 1)
#     pyplot.imshow(val_image[i], cmap=pyplot.get_cmap('gray'))
#     print(np.argmax(model.predict(val_image[i:i+1])))
#
# pyplot.show()

model_list = [model1_1, model1_256, model1_full_batch, model2, model3, model4, model5, model6, model7, model8]


training_loss_plots = {}
training_accuracy_plots = {}
validation_loss_plots = {}
validation_accuracy_plots = {}

for model in model_list:
    training_loss_plots[model] = model.history.history["loss"]
    training_accuracy_plots[model] = model.history.history["sparse_categorical_accuracy"]
    validation_loss_plots[model] = model.history.history["val_loss"]
    validation_accuracy_plots[model] = model.history.history["val_sparse_categorical_accuracy"]

legend_list = ["model1_1", "model1_256", "model1_full_batch", "model2", "model3",
               "model4", "model5", "model6", "model7", "model8"]

for plot in training_loss_plots:
    pyplot.plot(training_loss_plots[plot])
    pyplot.xlabel('Epoch')
    pyplot.ylabel('Training loss')

pyplot.legend(legend_list)
pyplot.yscale("log")
pyplot.grid()
pyplot.savefig('training_loss.eps')
pyplot.show()

for plot in training_accuracy_plots:
    pyplot.plot(training_accuracy_plots[plot])
    pyplot.xlabel('Epoch')
    pyplot.ylabel('Training accuracy')

pyplot.legend(legend_list)
pyplot.yscale("linear")
pyplot.grid()
pyplot.savefig('training_accuracy.eps')
pyplot.show()

for plot in validation_loss_plots:
    pyplot.plot(validation_loss_plots[plot])
    pyplot.xlabel('Epoch')
    pyplot.ylabel('Validation loss')

pyplot.legend(legend_list)
pyplot.yscale("log")
pyplot.grid()
pyplot.savefig('validation_loss.eps')
pyplot.show()

for plot in validation_accuracy_plots:
    pyplot.plot(validation_accuracy_plots[plot])
    pyplot.xlabel('Epoch')
    pyplot.ylabel('Validation accuracy')

pyplot.legend(legend_list)
pyplot.yscale("linear")
pyplot.grid()
pyplot.savefig('validation_accuracy.eps')
pyplot.show()

