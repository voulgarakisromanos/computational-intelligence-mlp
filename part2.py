import keras_tuner
from keras.datasets import mnist
import numpy as np
import keras_tuner as kt
from keras import backend as K
import sklearn
from sklearn import metrics
from tensorflow import keras
from matplotlib import pyplot
import tensorflow as tf
import tensorflow_addons as tfa

read_dictionary = np.load('best_hyperparameters.npy', allow_pickle='TRUE').item()

(train_image, train_class), (val_image, val_class) = mnist.load_data()

extra_val_image = train_image[0:12000]
extra_val_class = train_class[0:12000]
np.append(val_image, extra_val_image)
np.append(val_class, extra_val_class)
train_image_used = train_image[12000:]
train_class_used = train_class[12000:]
epoch_number = 100

# def build_model(hp):
#     regularizer = tf.keras.regularizers.L2(hp.Choice('regularizer_parameter', values=[0.1, 0.001, 0.000001]))
#     initializer = tf.keras.initializers.HeNormal()
#
#     model = tf.keras.models.Sequential([
#         tf.keras.layers.Flatten(input_shape=(28, 28)),
#         tf.keras.layers.Dense(hp.Choice('first_layer_neurons', values=[64, 128]),
#                               activation='relu', kernel_regularizer=regularizer, kernel_initializer=initializer),
#         tf.keras.layers.Dense(hp.Choice('second_layer_neurons', values=[128, 256]),
#                               activation='relu', kernel_regularizer=regularizer, kernel_initializer=initializer),
#         tf.keras.layers.Dense(10, activation='softmax', kernel_regularizer=regularizer)
#     ])
#
#     model.compile(
#         optimizer=tf.keras.optimizers.RMSprop(learning_rate=hp.Choice('learning_rate', values=[0.1, 0.01, 0.001])),
#         loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
#     )
#
#     return model
#
# tuner = kt.RandomSearch(
#     build_model,
#     objective='val_loss',
#     max_trials=1000, overwrite=True)
#
# stop_early = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=20)
#
# tuner.search(train_image_used, train_class_used, epochs=1000, validation_data=(val_image, val_class),
#              callbacks=[stop_early], validation_split=0.2)
# best_model = tuner.get_best_models()[0]
# best_hp = tuner.get_best_hyperparameters()[0]
# hp_array = {'first_layer': best_hp.get('first_layer_neurons'),
#             'second_layer': best_hp.get('second_layer_neurons'),
#             'regularizer_parameter': best_hp.get('regularizer_parameter'),
#             'learning_rate': best_hp.get('learning_rate')}
# np.save('best_hyperparameters.npy', hp_array)

# model.fit(train_image_used, train_class_used, batch_size=1, epochs=5, validation_data=(val_image, val_class))


regularizer = tf.keras.regularizers.l2(0.001)
initializer = tf.keras.initializers.he_normal()

model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(units=64, activation='relu', kernel_regularizer=regularizer, kernel_initializer=initializer),
    tf.keras.layers.Dense(units=128, activation='relu', kernel_regularizer=regularizer, kernel_initializer=initializer),
    tf.keras.layers.Dense(10, activation='softmax', kernel_regularizer=regularizer)
])

model.compile(
    optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
)

callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=200)

history = model.fit(train_image_used, train_class_used, batch_size=256, epochs=1000, validation_data=(val_image, val_class), callbacks=[callback])

predictions = np.argmax(model.predict(val_image), axis=1)
labels = val_class
sklearn.metrics.classification_report
print(sklearn.metrics.classification_report(labels, predictions))

pyplot.plot(history.history["loss"])
pyplot.xlabel('Epoch')
pyplot.ylabel('Training loss')
pyplot.yscale("log")
pyplot.grid()
pyplot.savefig('training_loss_best_model.eps')
pyplot.show()

pyplot.plot(history.history["val_loss"])
pyplot.xlabel('Epoch')
pyplot.ylabel('Validation loss')
pyplot.yscale("log")
pyplot.grid()
pyplot.savefig('validation_loss_best_model.eps')
pyplot.show()
